<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Errore</title>
</head>
<body>
	<%
		String tipo_errore = request.getParameter("tipo_errore") != null ? request.getParameter("tipo_errore") : "";
	%>

    <div class="container">

        <div class="row mt-5">
            <div class="col">
                <h1>
					<%
						switch(tipo_errore){
							case "NOGET":
								out.println("Errore, GET non permessa!");
								break;
							case "NOUSER":
								out.println("Errore, utente non trovato!");
								break;
							case "NOROLE":
								out.println("Errore, ruolo non definito!");
								break;
							case "NOTALLOWED":
								out.println("Errore, non puoi accedere qui!");
								break;
							case "NOTEXECUTED":
								out.println("Errore, Operazione non eseguita!");
								break;
							case "CAMPIVUOTI":
								out.println("Errore, CAMPO VUOTI!");
								break;
							case "ESISTE":
								out.println("Errore, UTENTE GIA REGISTRATO!");
								break;
							default:
								out.println("Errore generico!");
						}
					%>
				</h1>
				
				<button type="button" class="btn btn-info" onclick="javascript: history.go(-1);">Torna indietro</button>
            </div>
        </div>
        
    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>