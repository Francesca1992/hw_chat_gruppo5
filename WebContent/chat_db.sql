Drop DATABASE IF EXISTS CHATDB;
CREATE DATABASE CHATDB;
USE CHATDB;

CREATE TABLE utente(
	utenteId INTEGER NOT NULL AUTO_INCREMENT,
    username varchar(50) NOT NULL UNIQUE,
    pass varchar(20) not null,
    PRIMARY KEY (utenteId)

);

CREATE TABLE messaggio (
	messaggioId Integer not null AUTO_INCREMENT PRIMARY KEY,
    testo TEXT not null,
    orario datetime DEFAULT current_timestamp ,
    utenteref integer not null,
	FOREIGN KEY (utenteref) REFERENCES utente(utenteId)
);
INSERT INTO utente(username,pass) values 
("florian","ntt"),
("francesca","ntt");

INSERT INTO MESSAGGIO (testo,orario,utenteref) value
 ("ciao francesca","2020-01-24 05:53:06",1),
  ("ciao florian","2020-01-25 05:53:06",2),
  ("ciao DI NUOVO",CURRENT_TIMESTAMP,1);
  


SELECT *FROM UTENTE;
SELECT * FROM MESSAGGIO;
SELECT CURRENT_TIMESTAMP;