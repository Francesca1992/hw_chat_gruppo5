<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>
    
    
    <% 
    HttpSession sessione = request.getSession();
    boolean autorizzato =sessione.getAttribute("Auth") != null ? (boolean)sessione.getAttribute("Auth") : false;
    
    if(!autorizzato){
    	response.sendRedirect("errore.jsp?tipo_errore=NOTALLOWED");
    }
   
    %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>LIVE CHAT</title>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light " style="background-color:grey !important">
        <a class="navbar-brand" href="index.html">Live Chat</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link active" href="login.html">login</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="Chatroom.jsp">CHATROOM</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/hw_chat_gruppo5/logout">logout</a>
            </li>
            </ul>
        </div>
    </nav>
    
    <div class="container">
    
        <div class="row mt-2">
            <div class=col-md-3></div>
            <div class="col-md-6">
                <form action ="gestiscimessaggi" method ="post">
                    <div class="form-group">
                      <label for="textsms">Live Chat</label>
                      <textarea class="form-control" id="contenutotextarea" name="textsms" rows="10"></textarea>
                      
                    </div>
                    <div class="form-group">
                        <input type="text" name ="contenutoMessaggio"/> 
                        <button type="submit" name="mandaMessaggio-btn">invia messaggio</button> 
                       
                    </div>

                 </form>

            </div>
            <div class=col-md-3></div>
       
      	  </div>
       </div>



	
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>    
	<script src = "js/scriptChatroom.js"></script>
</body>
</html>