IL Progetto e stato ristrutturato nel seguente modo :

frontend: index.html login.html chatroom.jsp errore.jsp 
backend : implementazione attraverso servlet


Come frontend abbiamo la pagina index che contiene il form per la registrazione, una volta completato il form 
si viene reindirizzati alla servlet registrazione.java tramite 
la forma action con il metodo POST. In questa servlet vengono 
controllati i campi ricevuti e attraverso il campo username 
controlla se l utente esiste e nel caso esiste 
si viene indirizzati alla pagina di errore.jsp 
con il codice errore opportuno.

nel caso non esista viene creato l utente e inserito nel db. e successivamente si viene reindirizzati alla pagina di login.html.
qui e prensente il form per fare il login e dopo avere inserito i dati si viene reindirizzati alla servlet Controllalogin.java
 per controllare i campi ed eseguire un checkUtente sul database con i campi username e password. 
 se corrispondono a un record sul db viene creato una sessione con 2 attributi :sessione.setAttribute("nomeutente", inputUsername);
sessione.setAttribute("Auth", true); e si viene indirizzati alla pagina di chatroom.jsp.

in questa pagina si fa un ulteriore controllo sulla sessione se l utente e loggato per potere visualizzare la pagina.

L'utente puo visualizzare tutti i messaggi sulla chat e lui stesso puo scrivere un nuovo messaggio. quando si clicca invia si viene indirizzati alla servlet gestiscimessaggi.java
dove si prende il messaggio  passato attraverso il post e username ottenuto dalla sessione creata precedentemente. qui si fa un controllo se l utente esiste prima di inserire il messaggio.
dopo che viene inserito il messaggio si viene di nuovo indirizzati alla pagina Chatroom.jsp per vedere tutti i messaggi.
la pagina chatroom.jsp viene riempito a un intervallo di 2 secondi circa attraverso una funzione setinterval che chiama una funzione ajax ogni 3 secondi.


dopo che l utente ha chattato con la persona puo effettuare il log out dove la sua sessione viene invalidata e quindi non potra piu visualizzare la pagina della chat.


 