package com.hwchat.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.hwchat.connessione.ConnettoreDB;
import com.hwchat.model.Utente;
import com.mysql.jdbc.PreparedStatement;

public class UtenteDAO implements Dao<Utente> {

	@Override
	public Utente getById(int id) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
        
       	String query = "SELECT utenteID, username, pass FROM Utente WHERE utenteId = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setInt(1, id);
       	
       	
       	ResultSet risultato = ps.executeQuery();
       	Utente ut = new Utente();
    	
       	if(risultato.next()) {
       		ut.setUsername(risultato.getString(2));
       	}
		return ut;
	}

	@Override
	public ArrayList<Utente> getAll() throws SQLException {
		
		ArrayList<Utente> elencoUtenti = new ArrayList<Utente>();
		
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
        
       	String query = "SELECT utenteID, username, pass FROM Utente";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ResultSet risultato = ps.executeQuery();
       	
       	while(risultato.next()) {
       		Utente temp = new Utente();
       		temp.setUtenteID(risultato.getInt(1));
       		temp.setUsername(risultato.getString(2));
           	elencoUtenti.add(temp);
       	}
       	
       	return elencoUtenti;
	}

	@Override
	public void insert(Utente t) throws SQLException {
		
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		
		String query = "INSERT INTO Utente (username, pass) VALUE (?,?)";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
       	ps.setString(1, t.getUsername());
       	ps.setString(2, t.getPassword());
       	
       	ps.executeUpdate();
       	ResultSet risultato = ps.getGeneratedKeys();
       	risultato.next();
     
       	
       	t.setUtenteID(risultato.getInt(1));
	}

	@Override
	public boolean delete(Utente t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean update(Utente t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	public Integer checkUsername(String inputUsername) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
        
       	String query = "SELECT utenteID, username, pass FROM Utente WHERE username = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setString(1, inputUsername);
       	
       	
       	ResultSet risultato = ps.executeQuery();
       	Utente ut = new Utente();
       	ut.setUtenteID(-1);
       	
       	if(risultato.next()) {
       		ut.setUtenteID(risultato.getInt(1));
       	}
		return ut.getUtenteID();
	}
	
	public boolean checkUtente(Utente U) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
        
       	String query = "SELECT utenteID, username, pass FROM Utente WHERE username = ? && pass = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setString(1, U.getUsername());
       	ps.setString(2, U.getPassword());
       	ResultSet risultato = ps.executeQuery();
       	if(risultato.next()) {
       		return true;
       	}
		return false;
	}
	

}
