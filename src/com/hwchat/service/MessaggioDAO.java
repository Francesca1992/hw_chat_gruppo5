package com.hwchat.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;

import com.hwchat.connessione.ConnettoreDB;
import com.hwchat.model.Messaggio;
import com.mysql.jdbc.PreparedStatement;

public class MessaggioDAO implements Dao<Messaggio> {

	@Override
	public Messaggio getById(int id) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<Messaggio> getAll() throws SQLException {
		
		ArrayList<Messaggio> elencoMessaggi = new ArrayList<Messaggio>();
		
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
        
       	String query = "SELECT messaggioID, testo, orario, utenteRef FROM Messaggio";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ResultSet risultato = ps.executeQuery();
       	
       	while(risultato.next()) {
       		Messaggio temp = new Messaggio();
       		temp.setMessaggioID(risultato.getInt(1));
       		temp.setTesto(risultato.getString(2));
       		temp.setOrario(risultato.getTimestamp(3).getTime());
       		temp.setUtenteRef(risultato.getInt(4));
           	
           	elencoMessaggi.add(temp);
       	}
       	
       	return elencoMessaggi;
	}

	private Instant convertMilliToInstant(long time) {
		Instant instant = Instant.ofEpochMilli(time);
//		ZoneId zoneId = ZoneId.of( "europe/rome" ) ;
//	    ZonedDateTime zdt = ZonedDateTime.ofInstant( instant , zoneId ) ;
		return instant;
	}

	@Override
	public void insert(Messaggio t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		
		String query = "INSERT INTO Messaggio(testo,utenteref) value(?,?)";
		
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setString(1,t.getTesto());
		ps.setInt(2, t.getUtenteRef());
		
		int affrows = ps.executeUpdate();
		if(affrows>=1) {
			System.out.println("inserito!");
			
		}else {
			System.out.println("messaggio non inserito affected rows<1");
			
		}
		
		
		
	}

	@Override
	public boolean delete(Messaggio t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean update(Messaggio t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

}
