package com.hwchat.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.hwchat.model.Messaggio;
import com.hwchat.service.MessaggioDAO;

/**
 * Servlet implementation class Chatroom
 */
@WebServlet("/chatroom")
public class Chatroom extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		HttpSession sessione = request.getSession();
		boolean autorizzato =sessione.getAttribute("Auth") != null ? (boolean)sessione.getAttribute("Auth") : false;
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		MessaggioDAO smstemp =null;
		ArrayList<Messaggio> elencoMessaggi = new ArrayList<Messaggio>();
		
	
		try {
			if(autorizzato) {
				smstemp = new MessaggioDAO();
				elencoMessaggi = smstemp.getAll();
				out.print(new Gson().toJson(elencoMessaggi));
		
			}else {
				response.sendRedirect("errore.jsp?tipo_errore=NOTALLOWED");
			}
			
		} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
			
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		doGet(request, response);
	}

}
