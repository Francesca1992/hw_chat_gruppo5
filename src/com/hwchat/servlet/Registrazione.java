package com.hwchat.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hwchat.model.Utente;
import com.hwchat.service.UtenteDAO;

/**
 * Servlet implementation class Registrazione
 */
@WebServlet("/registrazione")
public class Registrazione extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("index.html");
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter pw = response.getWriter();
		String inputUsername = request.getParameter("inputUsername") != null ? (String)request.getParameter("inputUsername") : "";
		String inputPassword = request.getParameter("inputPassword") != null ? (String)request.getParameter("inputPassword") : "";
		
		
		UtenteDAO utenteD = new UtenteDAO();
		Utente temp = new Utente();
		if(!inputUsername.isBlank() && !inputPassword.isBlank()) {
			try {
				if(utenteD.checkUsername(inputUsername)!=-1) {
					response.sendRedirect("errore.jsp?tipo_errore=ESISTE");
					
				}else {
					temp.setUsername(inputUsername);
					temp.setPassword(inputPassword);
					
					utenteD.insert(temp);
					System.out.println(temp.stampaUtente());
					response.sendRedirect("login.html");
					
					
					
					
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println(e.getMessage());
			}
			
		}else {
			response.sendRedirect("errore.jsp?tipo_errore=CAMPIVUOTI");
		}
	}

}
