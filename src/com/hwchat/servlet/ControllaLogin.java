package com.hwchat.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.hwchat.model.Utente;
import com.hwchat.service.UtenteDAO;

/**
 * Servlet implementation class ControllaLogin
 */
@WebServlet("/controllalogin")
public class ControllaLogin extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("errore.jsp?tipo_errore=NOGET");
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter pw = response.getWriter();
		String inputUsername = request.getParameter("inputUsernameLogin") != null ? (String)request.getParameter("inputUsernameLogin") : "";
		String inputPassword = request.getParameter("inputPasswordLogin") != null ? (String)request.getParameter("inputPasswordLogin") : "";
	
		UtenteDAO ud = new UtenteDAO();
		Utente u = new Utente();
		HttpSession sessione = request.getSession();
		if(!inputUsername.isBlank() && !inputPassword.isBlank()) {
			u.setUsername(inputUsername);
			u.setPassword(inputPassword);
			try {
				if(ud.checkUtente(u)) {
					//response.sendRedirect("errore.jsp?tipo_errore=ESISTE");
					
					pw.print("<script> alert(\"utente loggato correttamente \"); </script>");
					sessione.setAttribute("nomeutente", inputUsername);
					sessione.setAttribute("Auth", true);
					response.sendRedirect("Chatroom.jsp");
					
					
					
				}else {
					pw.print("<script> alert(\"username e password errati\"); </script>");
					
						
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println(e.getMessage());
			}
			
		}else {
			response.sendRedirect("errore.jsp?tipo_errore=CAMPIVUOTI");
		}
		
		
	}
	

}
