package com.hwchat.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.hwchat.model.Utente;
import com.hwchat.service.UtenteDAO;

/**
 * Servlet implementation class CercaUtente
 */
@WebServlet("/cercautente")
public class CercaUtente extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		PrintWriter out = response.getWriter();
		HttpSession sessione = request.getSession();
		boolean autorizzato =sessione.getAttribute("Auth") != null ? (boolean)sessione.getAttribute("Auth") : false;
		
		ArrayList<Utente> tempU = new ArrayList<Utente>();
		try {
			if(autorizzato) {
				UtenteDAO ut= new UtenteDAO();
				tempU=ut.getAll();
				
				out.print(new Gson().toJson(tempU));
		
			}else {
				response.sendRedirect("errore.jsp?tipo_errore=NOTALLOWED");
			}
			
		} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		PrintWriter out = response.getWriter();
		HttpSession sessione = request.getSession();
		boolean autorizzato =sessione.getAttribute("Auth") != null ? (boolean)sessione.getAttribute("Auth") : false;
		Integer inputIdUtenteRef = request.getParameter("utenteId") != null ? 
				Integer.parseInt(request.getParameter("utenteId")):null;
		Utente tempU= new Utente();
		try {
			if(autorizzato && inputIdUtenteRef != null ) {
				UtenteDAO ut= new UtenteDAO();
				tempU=ut.getById(inputIdUtenteRef);
				
				out.print(new Gson().toJson(tempU));
		
			}else {
				response.sendRedirect("errore.jsp?tipo_errore=NOTALLOWED");
			}
			
		} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
	}

}
