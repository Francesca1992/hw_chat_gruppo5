package com.hwchat.servlet;
import java.time.*;
import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.hwchat.model.Messaggio;
import com.hwchat.service.MessaggioDAO;
import com.hwchat.service.UtenteDAO;

/**
 * Servlet implementation class GestisciMessaggi
 */
@WebServlet("/gestiscimessaggi")
public class GestisciMessaggi extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("errore.jsp?tipo_errore=NOGET");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String inputMessaggio = request.getParameter("contenutoMessaggio") != null ? 
				(String)request.getParameter("contenutoMessaggio"):null;
		HttpSession sessione = request.getSession();
		String username = (String)sessione.getAttribute("nomeutente");
		
		UtenteDAO tempDao = new UtenteDAO();
//		java.util.Date dt = new java.util.Date();
//
//		java.text.SimpleDateFormat sdf = 
//		     new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//
//		String currentTime = sdf.format(dt);
		
		//
		
		if(inputMessaggio != null) {
		Messaggio sms = new Messaggio();
		sms.setTesto(inputMessaggio);
		try {
			
			 int iduserTemp =tempDao.checkUsername(username);
			if(iduserTemp != -1) {
				sms.setUtenteRef(iduserTemp);
				MessaggioDAO Md = new MessaggioDAO();
				Md.insert(sms);
				response.sendRedirect("Chatroom.jsp");
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		//TODO:CONTINUARE
	
		}
	}

}
