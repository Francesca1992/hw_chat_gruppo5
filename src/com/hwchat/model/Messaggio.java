package com.hwchat.model;

import java.sql.Timestamp;
import java.time.Instant;

public class Messaggio {
	
	private Integer messaggioID;
	private String testo;
	private long orario;
	private Integer utenteRef;
	
	public Messaggio() {
		
	}

	public Integer getMessaggioID() {
		return messaggioID;
	}
	public void setMessaggioID(Integer messaggioID) {
		this.messaggioID = messaggioID;
	}
	public String getTesto() {
		return testo;
	}
	public void setTesto(String testo) {
		this.testo = testo;
	}
	public long getOrario() {
		return orario;
	}
	public void setOrario(long l) {
		this.orario = l;
	}
	public Integer getUtenteRef() {
		return utenteRef;
	}
	public void setUtenteRef(Integer utenteRef) {
		this.utenteRef = utenteRef;
	}

	public String stampaMessaggio() {
		return "Messaggio [messaggioID=" + messaggioID + ", testo=" + testo + ", orario=" + orario + ", utenteRef=" + utenteRef + "]";
	}
		
}