package com.hwchat.model;

public class Utente {
	
	private Integer utenteID;
	private String username;
	private String password;
	
	public Utente() {
		
	}
	
	public Integer getUtenteID() {
		return utenteID;
	}
	public void setUtenteID(Integer utenteID) {
		this.utenteID = utenteID;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String stampaUtente() {
		return "Utente [utenteID=" + utenteID + ", username=" + username + ", password=" + password + "]";
	}

}